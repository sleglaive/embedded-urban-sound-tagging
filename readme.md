# Embedded Urban Sound Tagging Project

<center>
<img width= 500 src='http://d33wubrfki0l68.cloudfront.net/282c08f73c870b0d68e92024a0248ac73d051daa/91ec9/images/tasks/challenge2016/task4_overview.png' />
</center>



## Introduction

In this project, you will develop an urban sound tagging system embedded on the [Nvidia Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit)  computing board. Given a ten-second audio recording from some urban environment, it will return whether each of the following eight predefined audio sources is audible or not:

1. engine    
2. machinery-impact   
3. non-machinery-impact    
4. powered-saw    
5. alert-signal    
6. music    
7. human-voice    
8. dog

This is a [multi-label classification](https://en.wikipedia.org/wiki/Multi-label_classification) problem.

You have two main objectives:

1. Build the system that obtains the best performance on the test set of the [SONYC Urban Sound Tagging (SONYC-UST) dataset](https://zenodo.org/record/2590742#.XIkTPBNKjuM).  

2. Make the best integration of your system on the [Nvidia Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit) in terms of latency and interface (controls, results display, etc.)

### Context

The city of New York, like many others, has a "noise code". For reasons of comfort and public health, jackhammers can only operate on weekdays; pet owners are held accountable for their animals' noises; ice cream trucks may play their jingles while in motion, but should remain quiet once they've parked; blasting a car horn is restricted to situations of imminent danger. The noise code presents a plan of legal enforcement and thus mitigation of harmful and disruptive types of sounds.

In an effort towards reducing urban noise pollution, the engagement of citizens is crucial, yet by no means sufficient on its own. Indeed, the rate of complaints that are transmitted, in a given neighborhood, through a municipal service such as 3-1-1, is not necessarily proportional to the level of noise pollution in that neighborhood. In the case of New York City, the Department of Environmental Protection is in charge of attending to the subset of noise complaints which are caused by static sources, including construction and traffic. Unfortunately, statistical evidence demonstrates that, although harmful levels of noise predominantly affect low-income and unemployed New Yorkers, these residents are the least likely to take the initiative of filing a complaint to the city officials. Such a gap between reported exposure and actual exposure raises the challenge of improving fairness, accountability, and transparency in public policies against noise pollution.

[![SONYC](https://i.ibb.co/0GSSfrr/https-i-ytimg-com-vi-d-JMt-VLUSEg-maxresdefault.jpg)](https://www.youtube.com/watch?v=d-JMtVLUSEg "SONYC")



### Motivation

Noise pollution is one of the topmost quality of life issues for urban residents in the United States. It has been estimated that 9 out of 10 adults in New York City are exposed to excessive noise levels, i.e. beyond the limit of what the EPA considers to be harmful. When applied to U.S. cities of more than 4 million inhabitants, such estimates extend to over 72 million urban residents.

The objectives of [SONYC](https://wp.nyu.edu/sonyc/) (Sounds of New York City) are to create technological solutions for: (1) the systematic, constant monitoring of noise pollution at city scale; (2) the accurate description of acoustic environments in terms of its composing sources; (3) broadening citizen participation in noise reporting and mitigation; and (4) enabling city agencies to take effective, information-driven action for noise mitigation.

SONYC is an independent research project for mitigating urban noise pollution. One of its aims is to map the spatiotemporal distribution of noise at the scale of a megacity like New York, in real time, and throughout multiple years. To this end, SONYC has designed an acoustic sensor for noise pollution monitoring. This sensor combines a relatively high accuracy in sound acquisition with a relatively low production cost. Between 2015 and 2019, over 50 different sensors have been assembled and deployed in various areas of New York City. Collectively, these sensors have gathered the equivalent of 37 years of audio data.

Every year, the SONYC acoustic sensor network records millions of such audio snippets. This automated procedure of data acquisition, in its own right, gives some insight into the overall rumble of New York City through time and space. However, as of today, each SONYC sensor merely returns an overall sound pressure level (SPL) in its immediate vicinity, without breaking it down into specific components. From a perceptual standpoint, not all sources of outdoor noise are equally unpleasant. For this reason, determining whether a given acoustic scene comes in violation of the noise code requires, more than an SPL estimate in decibels, a list of all active sources in the scene. In other words, in the context of automated noise pollution monitoring, the resort to computational methods for detection and classification of acoustic scenes and events (DCASE) appears as necessary.

## Project overview

During the development stage, you will work on [Google Colab](https://colab.research.google.com/notebooks/welcome.ipynb), an online computing environment based on [Jupyter](https://jupyter.org/) Notebooks. With Google Colab, you have access to free computational resources such as GPUs and TPUs, allowing you to efficiently train deep neural networks on large-scale datasets. 

**You will need about 5 Go of free space in a Google Drive.**

If you are not already familiar with Google Colab and Jupyter Notebooks, you can have a look to [this brief overview](https://colab.research.google.com/notebooks/basic_features_overview.ipynb).

In order to guide you in this project, you have access to the following Jupyter Notebooks. 

* `1-dataset.ipynb`: This notebook introduces the [SONYC-UST dataset](https://zenodo.org/record/2590742#.XIkTPBNKjuM). You will use this dataset in the development stage of your system, i.e. before deploying it on the [Nvidia Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit).

* `2-preliminaries.ipynb`: In this notebook, you will get familiar with:
    * how to manipulate and analyze the dataset;
    * how to read, write, play, and visualize audio files;
    * how to compute a log-Mel spectrogram from a raw audio waveform.

* `3-feature-extraction.ipynb`: In this notebook, you will extract the log-Mel spectrograms for the 3068 audio files in the SONYC-UST dataset.

* `4-model-training.ipynb`: In this notebook, you will build and train a convolutional neural network (CNN) to perform urban sound tagging, using [Keras](https://keras.io/).

* `5-model-testing.ipynb`: In this notebook, you will evaluate the performance of your trained CNN using standard metrics for multi-label classification. 

* `6-jetson-nano.ipynb`: In this notebook, you will learn some tools to help you embed your trained urban sound tagging system on the [Nvidia Jetson Nano](https://developer.nvidia.com/embedded/jetson-nano-developer-kit).

In each notebook, you may have to answer questions in *'text cells'*, or to write Python code in *'code cells'*. 

# License

* `utils.py` and `metrics.py` were taken and potentially modified from https://github.com/sonyc-project/urban-sound-tagging-baseline. They are licensed under the MIT license, Copyright (c) 2019 Sounds of New York City (SONYC).

* `mel_features.py` and `vggish_params.py`were taken and potentially modified from https://github.com/tensorflow/models/tree/master/research/audioset/vggish. They are licensed under the Apache License, Version 2.0, Copyright 2017 The TensorFlow Authors All Rights Reserved.

* All other files in this repository are licensed under the [GNU Affero General Public License (AGPL) 3.0](https://www.gnu.org/licenses/agpl-3.0.html), Copyright 2019 CentraleSupélec, author [Simon Leglaive](https://sleglaive.github.io).

  
  
  
